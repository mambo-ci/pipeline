# Copyright You-Sheng Yang
# SPDX-License-Identifier: FSFAP
# Copying and distribution of this file, with or without modification, are
# permitted in any medium without royalty provided the copyright notice and
# this notice are preserved. This file is offered as-is, without any warranty.

include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'

stages:
  - prerequisites
  - build
  - test
  - deploy
  - cleanup

variables:
  STAGING_IMAGE_PREFIX: "${CI_REGISTRY_IMAGE}/staging:${CI_PIPELINE_ID}-host-"
  IMAGE_PREFIX: "${CI_REGISTRY_IMAGE}/host:"

  GIT_DEPTH: 1

  REG_SHA256: "ade837fc5224acd8c34732bf54a94f579b47851cc6a7fd5899a98386b782e228"
  REG_VERSION: "v0.16.1"

check license + contributor:
  stage: prerequisites
  image: debian
  before_script:
    - apt-get update && apt-get install -y licensecheck
  script:
    - problems=0
    # check files without any license
    - bad_files=$(licensecheck -r . |
          grep -Ev '(README.md)|(CONTRIBUTORS)' | grep UNKNOWN) || true
    - |
      [ -z "${bad_files}" ] || \
          (echo "ERROR: Missing license statement in the following files:"; \
              echo "${bad_files}"; exit 1) || \
          problems=$((${problems} + 1))
    # check email or name is in the list of contributors
    - |
      grep -q "${GITLAB_USER_EMAIL}" CONTRIBUTORS || \
          grep -q "${GITLAB_USER_NAME}" CONTRIBUTORS || \
          (echo "ERROR: ${GITLAB_USER_NAME} <${GITLAB_USER_EMAIL}> missing in the CONTRIBUTORS file"; exit 1) || \
          problems=$((${problems} + 1))
    - exit ${problems}

tools versions:
  stage: prerequisites
  image: alpine
  script:
    # Check helm
    - helm_version=$(awk -F= '/^ARG HELM_VERSION=/ { print $2 }' 'kind/Dockerfile')
    - helm_latest_version=$(
          wget --no-verbose -O - "https://api.github.com/repos/helm/helm/releases/latest" |
          grep '"tag_name":' |
          sed -E 's/.*"([^"]+)".*/\1/')
    - helm_latest_sha256sum=$(
          wget --no-verbose -O - "https://get.helm.sh/helm-${helm_latest_version}-linux-amd64.tar.gz.sha256sum" |
          awk '{print $1}')

    # Check kind
    - kind_version=$(awk -F= '/^ARG KIND_VERSION=/ { print $2 }' 'kind/Dockerfile')
    - kind_latest_version=$(
          wget --no-verbose -O - "https://api.github.com/repos/kubernetes-sigs/kind/releases/latest" |
          grep '"tag_name":' |
          sed -E 's/.*"([^"]+)".*/\1/')
    - kind_latest_sha256sum=$(
          wget --no-verbose -O - "https://github.com/kubernetes-sigs/kind/releases/download/${kind_latest_version}/kind-linux-amd64" |
          sha256sum - |
          awk '{print $1}')

    # Check kubectl
    - kubectl_version=$(awk -F= '/^ARG KUBECTL_VERSION=/ { print $2 }' 'kind/Dockerfile')
    - kubectl_latest_version=$(
          wget --no-verbose -O - "https://dl.k8s.io/release/stable.txt")
    - kubectl_latest_sha256sum=$(
          wget --no-verbose -O - "https://dl.k8s.io/release/${kubectl_latest_version}/bin/linux/amd64/kubectl.sha256")

    # Check yq
    - yq_version=$(awk -F= '/^ARG YQ_VERSION=/ { print $2 }' 'kind/Dockerfile')
    - yq_latest_version=$(
          wget --no-verbose -O - "https://api.github.com/repos/mikefarah/yq/releases/latest" |
          grep '"tag_name":' |
          sed -E 's/.*"([^"]+)".*/\1/')
    - yq_latest_sha256sum=$(
          wget --no-verbose -O - "https://github.com/mikefarah/yq/releases/download/${yq_latest_version}/checksums" |
          awk '/^yq_linux_amd64/ {print $19}')

    # Check reg
    - reg_latest_version=$(
          wget --no-verbose -O - "https://api.github.com/repos/genuinetools/reg/releases/latest" |
          grep '"tag_name":' |
          sed -E 's/.*"([^"]+)".*/\1/')
    - reg_latest_sha256sum=$(
          wget --no-verbose -O - "https://github.com/genuinetools/reg/releases/download/${reg_latest_version}/reg-linux-amd64.sha256" |
          awk '{print $1}')

    - |
      echo "[Helm] latest: ${helm_latest_version}, sha256sum: ${helm_latest_sha256sum}, mine: ${helm_version}"
      echo "[KinD] latest: ${kind_latest_version}, sha256sum: ${kind_latest_sha256sum}, mine: ${kind_version}"
      echo "[kubectl] latest: ${kubectl_latest_version}, sha256sum: ${kubectl_latest_sha256sum}, mine: ${kubectl_version}"
      echo "[yq] latest: ${yq_latest_version}, sha256sum: ${yq_latest_sha256sum}, mine: ${yq_version}"
      echo "[reg] latest: ${reg_latest_version}, sha256sum: ${reg_latest_sha256sum}, mine: ${REG_VERSION}"

    - test "${helm_latest_version}" = "${helm_version}"
          && test "${kind_latest_version}" = "${kind_version}"
          && test "${kubectl_latest_version}" = "${kubectl_version}"
          && test "${yq_latest_version}" = "${yq_version}"
  allow_failure: true

.build-host-image: &build-host-image
  stage: build
  image: docker:git
  services:
    - docker:dind
  variables:
    # JOB_BASE_IMAGE:
    # JOB_HELM_VERSION:
    # JOB_HELM_SHA256SUM:
    # JOB_KIND_VERSION:
    # JOB_KIND_SHA256SUM:
    # JOB_KUBECTL_VERSION:
    # JOB_KUBECTL_SHA256SUM:
    # JOB_YQ_VERSION:
    # JOB_YQ_SHA256SUM:
  before_script:
    - docker login -u "${CI_REGISTRY_USER}" -p "${CI_JOB_TOKEN}" "${CI_REGISTRY}"
  script:
    - |
      export series=$(echo "${CI_JOB_NAME}" | cut -d: -f2)
      export tag="${STAGING_IMAGE_PREFIX}${series}"
    - mkdir -p "output/jobs/${CI_JOB_NAME}"

    # Build host docker image
    - (cd kind; docker build --tag "${tag}"
          ${JOB_BASE_IMAGE:+--build-arg BASE_IMAGE=${JOB_BASE_IMAGE}}
          ${JOB_DOCKER_VERSION:+--build-arg DOCKER_VERSION=${JOB_DOCKER_VERSION}}
          ${JOB_HELM_VERSION:+--build-arg HELM_VERSION=${JOB_HELM_VERSION}
                              --build-arg HELM_SHA256SUM=${JOB_HELM_SHA256SUM}}
          ${JOB_KIND_VERSION:+--build-arg KIND_VERSION=${JOB_KIND_VERSION}
                              --build-arg KIND_SHA256SUM=${JOB_KIND_SHA256SUM}}
          ${JOB_KUBECTL_VERSION:+--build-arg KUBECTL_VERSION=${JOB_KUBECTL_VERSION}
                                 --build-arg KUBECTL_SHA256SUM=${JOB_KUBECTL_SHA256SUM}}
          ${JOB_YQ_VERSION:+--build-arg YQ_VERSION=${JOB_YQ_VERSION}
                            --build-arg YQ_SHA256SUM=${JOB_YQ_SHA256SUM}}
          .)
    - docker push "${tag}"

build:devel:
  extends: .build-host-image
  variables:
    JOB_BASE_IMAGE: "vicamo/docker:devel-git"

build:hirsute:
  extends: .build-host-image
  variables:
    JOB_BASE_IMAGE: "vicamo/docker:hirsute-git"

build:groovy:
  extends: .build-host-image
  variables:
    JOB_BASE_IMAGE: "vicamo/docker:groovy-git"

build:focal:
  extends: .build-host-image
  variables:
    JOB_BASE_IMAGE: "vicamo/docker:focal-git"

build:bionic:
  extends: .build-host-image
  variables:
    JOB_BASE_IMAGE: "vicamo/docker:bionic-git"

build:xenial:
  extends: .build-host-image
  variables:
    JOB_BASE_IMAGE: "vicamo/docker:xenial-git"

build:sid:
  extends: .build-host-image
  variables:
    JOB_BASE_IMAGE: "vicamo/docker:sid-git"

build:bullseye:
  extends: .build-host-image
  variables:
    JOB_BASE_IMAGE: "vicamo/docker:bullseye-git"

build:buster:
  extends: .build-host-image
  variables:
    JOB_BASE_IMAGE: "vicamo/docker:buster-git"

build:stretch:
  extends: .build-host-image
  variables:
    JOB_BASE_IMAGE: "vicamo/docker:stretch-git"

build:alpine:
  extends: .build-host-image
  variables:
    JOB_BASE_IMAGE: "docker:git"

.child-pipeline-base: &child-pipeline-base
  stage: test
  variables:
    # MAMBO_CI_HOST_SERIES:
    # GitLab may only expand it once, so must expand STAGING_IMAGE_PREFIX here.
    MAMBO_CI_IMAGES_HOST: ${CI_REGISTRY_IMAGE}/staging:${CI_PIPELINE_ID}-host-${MAMBO_CI_HOST_SERIES}

test:non-merge-request:
  extends: .child-pipeline-base
  trigger:
    include:
      - .pipeline-child-base.yml
    strategy: depend
  variables:
    MAMBO_CI_HOST_SERIES: "groovy"
  needs:
    - job: build:groovy

.child-pipeline: &child-pipeline
  extends: .child-pipeline-base
  trigger:
    include:
      - .pipeline-child.yml
    strategy: depend

test:devel:
  extends: .child-pipeline
  variables:
    MAMBO_CI_HOST_SERIES: "devel"
  needs:
    - job: build:devel
      artifacts: false

test:hirsute:
  extends: .child-pipeline
  variables:
    MAMBO_CI_HOST_SERIES: "hirsute"
  needs:
    - job: build:hirsute

test:groovy:
  extends: .child-pipeline
  variables:
    MAMBO_CI_HOST_SERIES: "groovy"
  needs:
    - job: build:groovy

test:focal:
  extends: .child-pipeline
  variables:
    MAMBO_CI_HOST_SERIES: "focal"
  needs:
    - job: build:focal

test:bionic:
  extends: .child-pipeline
  variables:
    MAMBO_CI_HOST_SERIES: "bionic"
  needs:
    - job: build:bionic

test:xenial:
  extends: .child-pipeline
  variables:
    MAMBO_CI_HOST_SERIES: "xenial"
  needs:
    - job: build:xenial

test:sid:
  extends: .child-pipeline
  variables:
    MAMBO_CI_HOST_SERIES: "sid"
  needs:
    - job: build:sid

test:bullseye:
  extends: .child-pipeline
  variables:
    MAMBO_CI_HOST_SERIES: "bullseye"
  needs:
    - job: build:bullseye

test:buster:
  extends: .child-pipeline
  variables:
    MAMBO_CI_HOST_SERIES: "buster"
  needs:
    - job: build:buster

test:stretch:
  extends: .child-pipeline
  variables:
    MAMBO_CI_HOST_SERIES: "stretch"
  needs:
    - job: build:stretch

test:alpine:
  extends: .child-pipeline
  variables:
    MAMBO_CI_HOST_SERIES: "alpine"
  needs:
    - job: build:alpine

.deploy-template: &deploy-template
  image: docker:git
  services:
    - docker:dind
  dependencies: []
  variables:
    GIT_STRATEGY: none
  before_script:
    - docker login -u "${CI_REGISTRY_USER}" -p "${CI_JOB_TOKEN}" "${CI_REGISTRY}"

    - wget -O /usr/local/bin/reg "https://github.com/genuinetools/reg/releases/download/${REG_VERSION}/reg-linux-amd64"
    - echo "${REG_SHA256}  /usr/local/bin/reg" | sha256sum -c -
    - chmod a+x /usr/local/bin/reg
  script:
    - |
      tags=$(/usr/local/bin/reg tags --auth-url "${CI_REGISTRY}" \
                 -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" \
                 "${STAGING_IMAGE_PREFIX%:*}" | \
               grep "^${STAGING_IMAGE_PREFIX##*:}")
      for tag in ${tags}; do
        which=$(echo "${tag}" | cut -d- -f3-);
        staging_tag="${STAGING_IMAGE_PREFIX}${which}";
        tag="${IMAGE_PREFIX}${which}";
        echo "##### To push ${staging_tag} as ${tag} #####";
        test "${CI_JOB_STAGE}" = "deploy" || continue;

        docker pull "${staging_tag}";
        docker tag "${staging_tag}" "${tag}";
        docker push "${tag}";
        docker rmi "${staging_tag}" "${tag}";
      done

test:deploy:
  extends: .deploy-template
  stage: test

deploy:
  extends: .deploy-template
  stage: deploy
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

cleanup:
  stage: cleanup
  image: docker
  services:
    - docker:dind
  dependencies: []
  variables:
    GIT_STRATEGY: none
  before_script:
    - wget -O /usr/local/bin/reg "https://github.com/genuinetools/reg/releases/download/${REG_VERSION}/reg-linux-amd64"
    - echo "${REG_SHA256}  /usr/local/bin/reg" | sha256sum -c -
    - chmod a+x /usr/local/bin/reg
  script:
    - |
      tags=$(/usr/local/bin/reg tags --auth-url "${CI_REGISTRY}" \
                 -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" \
                 "${STAGING_IMAGE_PREFIX%:*}" | \
               grep "^${STAGING_IMAGE_PREFIX##*:}")
      for tag in ${tags}; do
        /usr/local/bin/reg rm -d --auth-url "${CI_REGISTRY}" \
            -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" \
            "${STAGING_IMAGE_PREFIX%:*}:${tag}" || true;
      done
  rules:
    - when: always

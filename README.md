# CI pipeline template for Kubernetes & Helm on GitLab

## Table of contents
* [Introduction](#introduction)
* [Basic Use](#basic-use)
* [Advanced Use](#advanced-use)
* [Hacking](#hacking)
* [Support](#support)
* [Copyright](#copyright)

## Introduction

Mambo CI is a habit project created to share CI pipeline definitions between Helm packages. It's inspired by Debian's [Salsa CI Pipeline](https://salsa.debian.org/salsa-ci-team/pipeline), CI templates for Debian packages, so it's also named from another kind of dancing.

So far the test pipeline is relying on [Kubernetes-in-Docker](https://github.com/kubernetes-sigs/kind), a.k.a. KinD, the only free Kubernetes implementation I know that just works in GitLab CI, but ideas to integrate others are still welcome.

## Basic Use

To use the Mambo CI Pipeline, the first thing to do is to enable the project's Pipeline. Go to `Settings` (General), expand `Visibility, project features, permissions`, and in `Repository`, enable `Pipelines`. This makes the `CI / CD` settings and menu available.

Then, change the project's setting to make it point to the pipeline's config file. This can be done on `Settings` -> `CI/CD` (on the expanded menu, don't click on the CI / CD rocket) -> `General Pipelines` -> `Custom CI config path`.

If the base pipeline configuration fits your needs without further modifications, the recommended way is to include `mambo-ci.yml` and `pipeline-jobs.yml` in your `.gitlab-ci.yml`:

```yaml
---
include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'

include:
  - https://gitlab.com/mambo-ci/pipeline/raw/master/mambo-ci.yml
  - https://gitlab.com/mambo-ci/pipeline/raw/master/pipeline-jobs.yml
```

Note that Mambo CI tries to preserve the flexibility to coexist with your own additional job definitions, so you should pick your own workflow as shown above.

## Advanced Use

Following the basic instructions will allow you to add all the building and testing stages, but customization of the scripts is possible.

The [`mambo-ci.yml`](https://gitlab.com/mambo-ci/pipeline/blob/master/mambo-ci.yml) template delivers the jobs definitions. Including only this file, no job will be added to the pipeline. On the other hand, [`pipeline-jobs.yml`](https://gitlab.com/mambo-ci/pipeline/blob/master/pipeline-jobs.yml) includes all the jobs' instances.

### Changing testbed release

By default, everything will run on the `'hirsute'` suite. Changing the release is as easy as setting a `MAMBO_CI_HOST_SERIES` variable:

```yaml
---
include:
  - https://gitlab.com/mambo-ci/pipeline/raw/master/mambo-ci.yml
  - https://gitlab.com/mambo-ci/pipeline/raw/master/pipeline-jobs.yml

variables:
  MAMBO_CI_HOST_SERIES: 'alpine'
```

The following releases are currently supported:
* `alpine`
* Debian
  * `sid`
  * `bullseye`
  * `buster`
  * `stretch`
* Ubuntu
  * `devel`
  * `hirsute` (default)
  * `groovy`
  * `focal`
  * `bionic`
  * `xenial`

### Omit image revision checks

Mambo CI pipelines compare the value of environment variable `MAMBO_CI_OMIT_IMAGE_REVISION` to `MAMBO_CI_IMAGE_REVISION_REQUIRED` written in the pipeline definition at runtime. If the former is less than the latter, an error will be raised. This is to prevent running pipelines with incompatible images. One may assign `MAMBO_CI_OMIT_IMAGE_REVISION_CHECKING` to any non-empty string to skip this check.

### Customize predefined per-job variables

Each job template may have their own set of customizable variables. One may override/set these variables as:

```yaml
---
include:
  - https://gitlab.com/mambo-ci/pipeline/raw/master/mambo-ci.yml

package:
  extends: .mambo-ci-package
  variables:
    JOB_HELM_ADDITIONAL_LINT_ARGS: '--set key1=val1'
```

The following variables are supported currently:

* .mambo-ci-package
  * `JOB_HELM_ADDITIONAL_LINT_ARGS`
  * `JOB_HELM_ADDITIONAL_PACKAGE_ARGS`
  * `JOB_HELM_REPO_INDEX_URL`
  * `JOB_ARTIFACT_HUB_METADATA_TEMPLATE`
* .mambo-ci-test
  * `JOB_KIND_CONFIG`
  * `JOB_KIND_ADDITIONAL_CREATE_FLAGS`
  * `JOB_KUBECTL_WAIT_NODE_TIMEOUT`
  * `JOB_KUBECTL_WAIT_POD_TIMEOUT`
  * `JOB_HELM_INSTALL_RELEASE`
  * `JOB_HELM_INSTALL_CHART`
  * `JOB_HELM_ADDITIONAL_INSTALL_ARGS`
  * `JOB_HELM_ADDITIONAL_TEST_ARGS`

### Customize predefined test job completely

One may want to customize predefined test job to add a `before_script` section
or for any other reason, but do not want to skip the whole `pipeline-jobs.yml`
to receive further revisions. To do so, use the `MAMBO_CI_SKIP_DEFAULT_TEST_JOB`
variable as following example:

```yaml
---
include:
  - https://gitlab.com/mambo-ci/pipeline/raw/master/mambo-ci.yml
  - https://gitlab.com/mambo-ci/pipeline/raw/master/pipeline-jobs.yml

variables:
  MAMBO_CI_SKIP_DEFAULT_TEST_JOB: 'alpine'

mytest:
  extends: .mambo-ci-test
  needs:
    - package
  before_script:
    - 'true'
```

## Hacking

### Allow images to be persistent

By default all images are deleted when the pipeline is finished. This is to avoid images bloating the staging docker repository, which often leads to all kinds of problems. One may manually cancel the `cleanup` job before it starts up, pull any of the just built images, and resume it whenever appropriate.

## Support

Just open an [issue here](https://gitlab.com/mambo-ci/pipeline/issues).

## Copyright

Icons made by [itim2101](https://www.flaticon.com/authors/itim2101) from [www.flaticon.com](https://www.flaticon.com/).
